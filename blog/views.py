from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from .models import Post
from .forms import PostForm

def index_view(request):
    url = reverse('post_detail', kwargs = {'pk': 1})
    print(url)
    return render(request, 'index.html', context = {
        'posts': post_list_view
    })

def post_list_view(request):
    post = Post.objects.all()
    return render(request, 'post_list.html', context={
        'posts' : post
    })

def post_view(request, *args, **kwargs):
    post = get_object_or_404(Post, pk=kwargs.get('pk'))
    return render(request, 'post_view.html',
                  context={'post': post})

def post_create_view(request):
    if request.method == 'GET':
        form = PostForm()
        return render(request, 'post_create.html', context={'form' : form })
    elif request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            new_post = Post.objects.create(
                title=form.cleaned_data['title'],
                body=form.cleaned_data['body'],
                author=form.cleaned_data['author'])
            
            return redirect('post_list')
        else:
            return render(request, 'post_create.html', context = {
                'form' : form})
    
def post_update_view(request, *args, **kwargs):
    post = get_object_or_404(Post, pk = kwargs.get('pk'))
    
    if request.method == 'GET':
        form = PostForm(initial={
            'title' : post.title,
            'author' : post.author,
            'body' : post.body
            })
        return render(request, 'post_update.html', context={'form':form, 'post':post})
    
    elif request.method == 'POST':
        form = PostForm(data=request.POST)
        
        if form.is_valid():
            post.title = form.cleaned_data.get('title')
            post.author = form.cleaned_data.get('author')
            post.body = form.cleaned_data.get('body')
            post.save()
            return redirect('post_detail', pk=post.pk)
        else:
            return render(request, 'post_update.html', context={'form' : form, 'post':post})

def post_delete_view(request, *args, **kwargs):
    post = get_object_or_404(Post, pk=kwargs.get('pk'))
    if request.method == 'GET':
        return render(request, 'post_delete.html', context={'post' : post})
    elif request.method == 'POST':
        post.delete()
        return redirect('post_list')