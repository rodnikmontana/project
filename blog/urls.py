from django.urls import path
from blog.views import (
    index_view,
    post_view,
    post_create_view,
    post_list_view,
    post_update_view,
    post_delete_view
)

urlpatterns = [
    path('details/<int:pk>/', post_view, name='post_detail'),
    path('create/', post_create_view, name='post_create'),
    path('list/', post_list_view, name='post_list'),
    path('update/<int:pk>/', post_update_view, name='post_update'),
    path('delete/<int:pk>/', post_delete_view, name='post_delete')
]