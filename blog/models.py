from django.db import models

class Post(models.Model):
    title = models.CharField(max_length = 200, null = False, blank = False, verbose_name = 'Заголовок')
    body = models.TextField(max_length = 3000, null = True, blank = True, verbose_name = 'Тело статьи')
    author = models.CharField(max_length = 100, null = False, blank = False, verbose_name = 'Автор')
    created_at = models.DateTimeField(auto_now_add = True, verbose_name = 'Дата создания')
    updated_at = models.DateTimeField(auto_now = True, verbose_name = 'Дата редактирования')

    def __str__(self):
        return f'{self.pk} - {self.title}'